.model small
.stack 100h
.data

    double db 0ah, 0dh, 0ah, 0dh, '$' 
   lineBreakHS db 0ah, 0dh, 0ah, 0dh, 0ah, 0dh, 0ah, 0dh, 0ah, 0dh, 0ah, 0dh, 0ah, 0dh, 0ah, 0dh,'$'
   lineBreakSC db 0ah, 0dh, 0ah, 0dh, 0ah, 0dh, '$'
   
    a db '                            Welcome in our project                $'
    b db '                        ********** Bachelor ********** $'    
    c db '                            press C to see service        $' 
    d db '                         Press SPACE to go to Home Screen    $'    
    
    m1 dw 10,13,10,13, "select menu:$"
    m2 dw 10,13,10,13, "1.Rice+fish 100/- 2.Rice+vegetable 50/- 3.Soup 20/- $"
    m3 dw 10,13,10,13, "Select the menu number:$"
    m8 dw 10,13,10,13, "There is no more than 3 item,if you want,you can add one$"
    m9 dw 10,13,10,13, "Enter Food name:$"
    m10 dw 10,13,10,13,"          Price:$" 
    m4 dw 10,13,10,13, "To add press 1 or press 2 to get back menu :$"
    m5 dw 10,13,10,13, "Enter quantity:$" 
    m6 dw 10,13,10,13, "Total price: $"
    m7 dw 10,13,10,13, "      *****THANK YOU*****$"
    m11 dw "4.$"  
    m12 dw "/-$" 
    m13 dw 10,13,10,13, " Re-odrer : Press <1>$",
    m14 dw 10,13,10,13, " Exit : Press Any key$" 
    q dw 0    
    r dw 0
    v db 0
    s dw 0
    rprice dw 100
    vprice dw 50
    sprice dw 20
    nprice dw 0
    
    var1 db 100 dup('$')
    
.code
    main proc
        
        mov ax,@data
        mov ds,ax
        
       
    mov ah, 00h
    mov al, 03h
    int 10h    
    
    lea dx, lineBreakHS
    mov ah, 9     
    int 21h
        
    
    lea dx, a
    mov ah, 9     
    int 21h 
    
    lea dx, double
    mov ah, 9     
    int 21h
    
    lea dx, b
    mov ah, 9     
    int 21h
    
    lea dx, double 
    mov ah, 9     
    int 21h
    
    lea dx, c
    mov ah, 9     
    int 21h  
    
   
    


     mov ah, 00h
     mov al, 03h
     int 10h    
    
     lea dx, lineBreakSC
     mov ah, 9     
     int 21h   
        
    start:
        cmp v,0
        jg start1
        
        mov ah,9
        Lea dx,m1
        int 21h
        
    menu:
        
        mov ah,9
        Lea dx,m2
        int 21h
        
        mov ah,9
        Lea dx,m3
        int 21h
        
        mov ah,1
        int 21h        
        
        cmp al,31h
        je rice
        cmp al,32h
        je veg
        cmp al,33h
        je soup
         
        
        
   menuadd:
        inc v
        
        mov ah,9
        Lea dx,m8
        int 21h 
        
        mov ah,9
        Lea dx,m4
        int 21h
        
        mov ah,1
        int 21h
        cmp al,32h
        je menu
                
        mov ah,9
        Lea dx,m9
        int 21h
        
        mov si,offset var1
        
     l1:
        mov ah,1
        int 21h
        cmp al,13
        je print
        mov [si],al
        inc si
        jmp l1
        
    print: 
        
        call price
        
   start1:
        
        mov ah,9
        Lea dx,m2
        int 21h
        
        mov ah,9
        Lea dx,m11
        int 21h  
        
        mov dx,offset var1
        mov ah,9
        int 21h
        
        mov ah,2
        mov dl,' '
        int 21h 
        
        xor ax,ax   
        mov ax,nprice
        call bill
        
        mov ah,9
        Lea dx,m12
        int 21h 
        
        
        
        mov ah,9
        Lea dx,m3
        int 21h
        
        mov ah,1
        int 21h        
        
        cmp al,31h
        je rice
        cmp al,32h
        je veg
        cmp al,33h
        je soup   
        
        
    proc bill
            
            push ax
            push bx
            push cx
            push dx 
            
            
            or ax,ax
            jge bill_1  
            
            
            push ax
            mov dl,' '
            mov ah,2
            int 21h
            pop ax
            neg ax
            
            
      bill_1:
            
            xor cx,cx
            mov bx,10d
            
            
      repeat:
            
            
            xor dx,dx
            div bx
            push dx
            inc cx
            
            
            or ax,ax
            jne repeat
            
            mov ah,2
            
            
            
     display:
            
            pop dx
            or dl,30h
            int 21h
            loop display
            
            
            pop dx
            pop cx
            pop bx
            pop ax
            
            ret
            endp bill   
        
        
        
        
        
   proc bill_2 
            
            push bx
            push cx
            push dx 
            
            
       start2:
            
            mov ah,2
            mov dl,' '
            int 21h
            
            
            xor bx,bx
            xor cx,cx
            
            mov ah,1
            int 21h
            
            cmp al,'-'
            je substrucion
            
            cmp al,'+'
            je sum
            jmp again 
            
            
            
       substrucion:
            
            mov cx,1
            
            
        sum:
            
            int 21h
            
            
       again:
            
            cmp al,'0'
            jnge character
            
            cmp al,'9'
            jnle character 
            
            and ax,000FH
            push ax
            
            
            mov ax,10
            mul bx
            pop bx
            add bx,ax
            
            
            mov ah,1
            int 21h 
            
            
            cmp al,0dh
            jne again 
            
            
            mov ax,bx
            or cx,cx
            
            je exit
            neg ax
            
            
        exit:
            pop dx
            pop cx
            pop bx
            ret
            
            
            
            
            
       character:
       
            mov ah,2
            mov dl,0dh
            int 21h
            
            mov dl,0ah
            int 21h
            jmp start2  
            
            
            
            endp bill_2
            
        
        
        addmenu:
        
            mov ah,9
            Lea dx,m5
            int 21h
            
            xor ax,ax            
            
            call bill_2
    
            mul nprice
            
            mov bx,ax
            
            jmp totalprice
         
        
        veg:
        
            mov ah,9
            Lea dx,m5
            int 21h
            
            xor ax,ax            
            
            call bill_2
    
            mul vprice
            
            mov bx,ax
            
            jmp totalprice
        
        rice:
        
            mov ah,9
            Lea dx,m5
            int 21h 
            
            xor ax,ax
            
            call bill_2
            
            mul rprice 
            
            mov bx,ax
            
            jmp totalprice 
            
        soup:    
            mov ah,9
            Lea dx,m5
            int 21h 
            
            xor ax,ax
            
            call bill_2
           
            mul sprice
            
            mov bx,ax
            
            jmp totalprice 
            
        price:
        
            mov ah,9
            Lea dx,m10
            int 21h  
            
            mov ax,0
             mov bx,0
              mov cx,0
               mov dx,0
            
            input:
                and ax,000Fh
                push ax
                mov ax,10
                mul bx
                mov bx,ax
                pop ax
                add bx,ax
                
                mov ah,1
                int 21h
                
                cmp al,0Dh
                jne input
                
            add nprice,bx
            ret
               
            
            
        totalprice:
         
            mov ah,9
            Lea dx,m6
            int 21h 
            
            xor ax,ax
            
            
            mov ax,bx
            call bill
            
            mov ah,9
            Lea dx,m13
            int 21h
            
            mov ah,9
            Lea dx,m14
            int 21h
            
            mov ah,1
            int 21h
            
            cmp al,31h
            je start
            
            mov ah,9
            Lea dx,m7
            int 21h
        
         lea dx, d         ;get back to homescreen
         mov ah, 9     
         int 21h 
    
   
    
        mov ah,4ch
        int 21h
        
        
        end main
        ;proc homeScreen
   
   
    
homeScreenLoop: 

        mov ah, 0
        int 16h
        
       
        cmp al, 43h            
            
            je call main
            
        cmp al, 63h            
            
            je call main
            
        
        jmp homeScreenLoop          
        
    
    ret
    
endp homeScreen 
    
        
    
    
        